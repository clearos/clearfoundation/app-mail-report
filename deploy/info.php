<?php

/////////////////////////////////////////////////////////////////////////////
// General information
/////////////////////////////////////////////////////////////////////////////

$app['basename'] = 'mail_report';
$app['version'] = '2.1.6';
$app['vendor'] = 'ClearFoundation';
$app['packager'] = 'ClearFoundation';
$app['license'] = 'GPLv3';
$app['license_core'] = 'LGPLv3';
$app['description'] = lang('mail_report_app_description');
$app['powered_by'] = array(
    'vendor' => array(
        'name' => 'Postfix',
        'url' => 'http://www.postfix.org/'
    ),
    'packages' => array(
        'postfix-perl-scripts' => array(
            'name' => 'Postfix Perl Scripts',
            'version' => '---',
        ),
    ),
);

/////////////////////////////////////////////////////////////////////////////
// App name and categories
/////////////////////////////////////////////////////////////////////////////

$app['name'] = lang('mail_report_app_name');
$app['category'] = lang('base_category_reports');
$app['subcategory'] = lang('base_subcategory_user_management');

/////////////////////////////////////////////////////////////////////////////
// Controllers
/////////////////////////////////////////////////////////////////////////////

$app['controllers']['mail_report']['title'] = $app['name'];
$app['controllers']['settings']['title'] = lang('base_settings');

/////////////////////////////////////////////////////////////////////////////
// Packaging
/////////////////////////////////////////////////////////////////////////////

$app['core_requires'] = array(
    'app-reports-core',
    'app-tasks-core',
    'postfix-perl-scripts',
);

$app['core_file_manifest'] = array(
    'app-mail-report.cron' => array('target' => '/etc/cron.d/app-mail-report')
);

